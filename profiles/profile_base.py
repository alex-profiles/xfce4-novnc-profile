import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab.pnext as pn
import geni.rspec.igext as ig


def make_profile(disk_image, setup=False):
    portal.context.defineParameter("node_type",
                                   "Type of node to get (e.g. d740, d840, "
                                   "etc)",
                                   portal.ParameterType.STRING, "d740")

    params = portal.context.bindParameters()
    request = portal.context.makeRequestRSpec()

    node = request.RawPC("node")
    node.hardware_type = params.node_type
    node.disk_image = disk_image

    if setup:
        node.addService(
            rspec.Execute(shell="bash",
                          command="/local/repository/setup_vnc.sh"))

    node.addService(
        rspec.Execute(shell="bash",
                      command="/local/repository/start_vnc.sh"))


    # noVNC
    node.startVNC(True)

    portal.context.printRequestRSpec()

