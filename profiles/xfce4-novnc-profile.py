"""
A stock image with XFCE4 available through noVNC.
"""

import profiles.profile_base

profiles.profile_base.make_profile(
    "urn:publicid:IDN+emulab.net+image+PowderTeam:xfce4-novnc-profile",
    setup=False)
