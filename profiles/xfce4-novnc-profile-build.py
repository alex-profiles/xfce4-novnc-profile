"""
Build image for XFCE4 noVNC Profile.
"""

import profiles.profile_base

profiles.profile_base.make_profile(
    "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU20-64-STD",
    setup=True)
